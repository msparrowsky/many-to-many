package com.mwroblewski.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class Person {

    @Id
    private Long id;
    private String firstname;
    private String lastname;
    private int age;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "person")
    private Set<Contract> contracts;

    public Person(){}

    public Person(Long id, String firstname, String lastname, int age) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Set<Contract> getContracts() {
        return contracts;
    }

    public void setContract(Set<Contract> contracts) {
        this.contracts = contracts;
    }
}
