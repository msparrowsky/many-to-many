package com.mwroblewski;

import com.mwroblewski.model.Contract;
import com.mwroblewski.model.Organisation;
import com.mwroblewski.model.Person;
import com.mwroblewski.repository.ContractRepository;
import com.mwroblewski.repository.OrganisationRepository;
import com.mwroblewski.repository.PersonRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Year;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Autowired
	private ContractRepository contractRepository;
	@Autowired
	private OrganisationRepository organisationRepository;
	@Autowired
	private PersonRepository personRepository;

	private Contract contract;
	private Organisation organisation;
	private Person person;

	@Before
	@Transactional
	public void setUp() {
		this.person = new Person(1l, "Adam", "Kowalski", 30);
		this.organisation = new Organisation(1l, "Adam Kowalski IT Sp.z.o.o");
		this.contract = new Contract(1l, LocalDate.now().minus(2l, ChronoUnit.YEARS), LocalDate.now().plus(2l, ChronoUnit.YEARS));

		// temporary instances of relations' members with only corresponding id
		Person tmpPerson = new Person();
		tmpPerson.setId(1l);
		Organisation tmpOrganisation = new Organisation();
		tmpOrganisation.setId(1l);
		this.contract.setPerson(tmpPerson);
		this.contract.setOrganisation(tmpOrganisation);

		this.personRepository.save(this.person);
		this.organisationRepository.save(this.organisation);
		this.contractRepository.save(contract);
	}

	@Test
	@Transactional
	public void manyToManyRelation() {
		Contract resultContract = this.contractRepository.getOne(1l);
		Person resultPerson = resultContract.getPerson();
		Organisation resultOrganisation = resultContract.getOrganisation();

		assertEquals(this.contract.getId(), resultContract.getId());
		assertEquals(this.contract.getStart(), resultContract.getStart());
		assertEquals(this.contract.getEnd(), resultContract.getEnd());

		assertEquals(this.person.getId(), resultPerson.getId());
		assertEquals(this.person.getFirstname(), resultPerson.getFirstname());
		assertEquals(this.person.getLastname(), resultPerson.getLastname());
		assertEquals(this.person.getAge(), resultPerson.getAge());

		assertEquals(this.organisation.getId(), resultOrganisation.getId());
		assertEquals(this.organisation.getName(), resultOrganisation.getName());
	}
}
